variable "name" {
  description = "Nome para sua rede"
  type        = string
}

variable "description"  {
  description = "Descrição para sua rede"
  type        = string
}

variable "auto_create_subnetworks" {
  description = "Redes devem criar automaticamente subredes?"
  type        = bool
  default     = true
}
